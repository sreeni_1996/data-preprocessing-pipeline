from nltk.stem import PorterStemmer

class Stemming:
    def __init__(self,text):
  # takes the input for stemming
        port = PorterStemmer()
        content =""
        text_list = self.text

        try:
            content_name = ' '.join \
                ([port.stem(m) for m in text_list.split()])  # splitting the text, stemming it, joining it again
            content =content_name
        except(TypeError):
            content = text_list
        return content